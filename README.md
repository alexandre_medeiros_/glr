### O que é? ###

* Página web que exibe dados metereológicos de cidades usando a API pública do openweather.com
* A cada 10 segundos a lista de cidades é atualizada.
* Um campo para buscar uma cidade na lista está disponível.

### Setup ###

* Faça o download do projeto e faça o deploy em seu servidor web de preferência. Utilizei o Apache Web server.
Ex.: http://localhost/glr/


### Contato ###

* medeirosalexandre@gmail.com