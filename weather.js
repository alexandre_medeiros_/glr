//Array com os resultados encontrados no uso da API
var arrResultados = [];


$('document').ready(function() {
    
    //Chamada da função que exibe as cidades em uma tabela
    randomiza();

    //Chamada da função que exibe as cidades em uma tabela a cada 10 segundos
    setInterval(
        randomiza
    ,
    10000);


});

/**
Função que exibe a previsão do tempo de 3 cidades em uma tabela
*/
function randomiza() {
    
    var items = [];
    items.push("London");
    items.push("Barcelona");
    items.push("Berlin");
    items.push("Miami");
    items.push("Detroit");
    items.push("Montreal");
    items.push("Sidney");
    items.push("Anchorage");
    items.push("Warsaw");
    items.push("Maranelo");
    items.push("New Orleans");
    items.push("Paris");
    items.push("Tokyo");
    items.push("Hong Kong");
    items.push("Quebec");
    items.push("Rio de Janeiro");
    items.push("Washington");
    items.push("Yokohama");
    items.push("Boston");
    items.push("Chicago");
    items.push("Roma");

    //Array com as cidades em que se deseja saber a previsão do tempo.
    //Serão pegos elementos aleatórios da lista de cidades acima para fazer a chamada da API da previsão do tempo
    var cidades = [];

    for (i = 0, j = 3; i < j; i++) {
        var item = items[Math.floor(Math.random()*items.length)];
        //Por segurança, verifica se a cidade já existe no array. Caso exista, faz um nova busca aleatória.
        if (cidades.indexOf(item) == -1) {
            cidades.push(item);
        }
        else {
            item = items[Math.floor(Math.random()*items.length)];
            cidades.push(item);
        }
    }


    var responses = [];
    var async_request = [];

    //Faz a chamada da API para as cidades contidas no array de cidades, retornando os dados de previsão do tempo
    for(i = 0, j = cidades.length; i < j; i++) {
        async_request.push($.ajax({
            url:'http://api.openweathermap.org/data/2.5/forecast?q=' + cidades[i] + '&mode=json&appid=005d2830054392477d792babaf9d7ad7',
            method:'get',
            success: function(data) {
                console.log('success of ajax response')
                responses.push(data);
            }
        }));
    }

    
    //Monta os dados que serão exibidos para o usuário em forma de tabela
    $.when.apply(null, async_request).done( function(){
        // all done
        console.log('all request completed')
        
        for(i = 0, j = responses.length; i < j; i++) {
            montaObjeto(responses[i]);
        }
        
        montaTabela(arrResultados);

        
        setTimeout(function(){ 
            jQuery('#tbodyid').empty(); 
            
            limpaArray(cidades);
            limpaArray(arrResultados);

        }, 8000);

        
    });



}

function limpaArray(array) {
    while (array.length > 0) {
        array.pop();
    }
}


/**
Função que monta os objetos que serão exibidos na tabela
*/
function montaObjeto(data) {
    var arrDatas = [];
    var arrDataTemp = [];
    var arrElementos = [];
    var elemento = {};

    
    /*
    Extrai os dados de previsão do tempo das cidades informadas.
    Pega a primeira ocorrência de determinada data, armazenando em um array
    */
    for(k = 0, l = data.list.length; k < l; k++) {
        var strData = data.list[k].dt_txt.substring(0,10);
        if (arrDatas.indexOf(strData) === -1) {
            arrDatas.push(strData);

            //Temperatura vem em Kelvin. Subtraindo 273.15 converte para celsius
            arrDataTemp.push(strData + " - " + (data.list[k].main.temp - 273.15).toFixed(2) + "°");
        }
    }

    //Extrai as datas que serão utilizadas como cabeçalho na tabela
    var d1 = arrDataTemp[0].substring(0,10);
    var d2 = arrDataTemp[1].substring(0,10);
    var d3 = arrDataTemp[2].substring(0,10);
    var d4 = arrDataTemp[3].substring(0,10);
    var d5 = arrDataTemp[4].substring(0,10);

    //Extrai das datas o mês e o dia para que sejam exibidos nos cabeçalhos das colunas
    var mesData1 = d1.substring(5,7);
    var diaData1 = d1.substring(8)

    var mesData2 = d2.substring(5,7);
    var diaData2 = d2.substring(8)

    var mesData3 = d3.substring(5,7);
    var diaData3 = d3.substring(8)

    var mesData4 = d4.substring(5,7);
    var diaData4 = d4.substring(8)

    var mesData5 = d5.substring(5,7);
    var diaData5 = d5.substring(8)

    
    //Cria o objeto contendo os dados extraídos da API
    elemento = {"Cidade" : data.city.name,
                "p1" : {"data":diaData1 + "/" + mesData1, "temp": arrDataTemp[0].substring(13)},
                "p2" : {"data":diaData2 + "/" + mesData2, "temp": arrDataTemp[1].substring(13)},
                "p3" : {"data":diaData3 + "/" + mesData3, "temp": arrDataTemp[2].substring(13)},
                "p4" : {"data":diaData4 + "/" + mesData4, "temp": arrDataTemp[3].substring(13)},
                "p5" : {"data":diaData5 + "/" + mesData5, "temp": arrDataTemp[4].substring(13)} 
            };

    //Insere no array o elemento que será exibido na tabela
    arrResultados.push(elemento);
    
}



/*
Monta a tabela com os dados extraídos da API
*/
function montaTabela(arrElementos) {

    var linhasTabela = "";

    for(i = 0, j = arrElementos.length; i < j; i++) {
        linhasTabela += "<tr>";
        linhasTabela += "<td>" + arrElementos[i].Cidade + "</td>" +
            "<td>" + arrElementos[i].p1.temp + "</td>" +
            "<td>" + arrElementos[i].p2.temp + "</td>" +
            "<td>" + arrElementos[i].p3.temp + "</td>" +
            "<td>" + arrElementos[i].p4.temp + "</td>" +
            "<td>" + arrElementos[i].p5.temp + "</td>" +
            "</tr>"
    }

    
    var container = document.getElementById("container");
    container.innerHTML = [
      '<div id="tabela"><table class="table table-striped table-hover" id="tabelaPrevisoes">',
          '<thead>',
              '<tr>',
                  '<th></th>',
                  '<th>' + arrElementos[0].p1.data + '</th>',
                  '<th>' + arrElementos[0].p2.data + '</th>',
                  '<th>' + arrElementos[0].p3.data + '</th>',
                  '<th>' + arrElementos[0].p4.data + '</th>',
                  '<th>' + arrElementos[0].p5.data + '</th>',
              '</tr>',
          '</thead>',
          '<tbody id="tbodyid">',
              linhasTabela,
          '</tbody>',
      '</table></div>'
    ].join("\n");
}




/*
Procura na tabela o nome da cidade que foi digitado no campo de pesquisa
*/
function buscaNaTabela() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("tfPesquisa");
    filter = input.value.toUpperCase();
    table = document.getElementById("tabelaPrevisoes");
    tr = table.getElementsByTagName("tr");

    
    //Faz um loop em todas as linhas da tabela, escondendo o que não corresponde com o que foi digitado
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        } 
    }
}


